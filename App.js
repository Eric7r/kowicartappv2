import React, {useMemo, useState,useEffect} from 'react';
import { StyleSheet, View,Text ,Button} from 'react-native';
import * as Notifications from 'expo-notifications'
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions'
import {Provider as PaperProvider} from 'react-native-paper'; 
import jwtDecode from "jwt-decode";
import AppNavigation from "./src/navigation/AppNavigation";
import AuthScreen from "./src/screens/Auth";
import AuthContext from "./src/context/AuthContext";
import axios from "axios";
import {  setTokenApi, getTokenApi, setIdUserApi , getIdUserApi , removeTokenApi} from "./src/api/token";

import { AppearanceProvider, useColorScheme } from 'react-native-appearance';

export default function App() {
  const [expoPushToken, setExpoPushToken] = useState('');
  const [idUsuario, setIdUsuario] = useState('');

  useEffect(() => {
    registerForPushNotificationsAsync().then(token2 => setExpoPushToken(token2));
    
  }, [])

  async function registerForPushNotificationsAsync() {
    let token2;
    if (Constants.isDevice) {
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token2 = (await Notifications.getExpoPushTokenAsync()).data;
  
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    } 
    return token2;
  }

  const [auth, setAuth] = useState(undefined);

  useEffect(() => {
    setAuth(null);
  
  }, []);

  useEffect(() => {
    (async () => {
      const token = await getTokenApi();
      const userid = await getIdUserApi();
      
      if (token) {

        setAuth({
          token,
          userid:jwtDecode(token).sub,
       
        });
      } else {
        setAuth(null);
      }
    })();
  }, []);

  useEffect(() => {
    (async () => {
      const token = await getTokenApi();

      if (token) {

        setIdUsuario(
          jwtDecode(token).sub,
        );
      } else {
        setIdUsuario(null);
      }
    })();
  }, []);




  const login = (user) => {

 setTokenApi(user.token);
 setIdUserApi(user.userid);

 setAuth({
 token:user.token,
 userid:user.userid,

 })
  };

  const logout =   () => {
  if(auth)
  {
    removeTokenApi();
    setAuth(null);
  } 
  }

  const  authData = useMemo(
    () => ({
      auth,
      login,
      logout,
      

    }),
    [auth]
  );

  if (auth === undefined) return null;

  return (
  <AuthContext.Provider value={authData}> 
   <PaperProvider> 
  {auth ? <AppNavigation/>: 
  <AuthScreen />}
    </PaperProvider>
    </AuthContext.Provider>
  );
}

const styles = StyleSheet.create({
  
});
