import React from 'react';
import { View, Text } from 'react-native';
import { createStackNavigator } from "@react-navigation/stack";
import LoginForm from '../components/Auth/LoginForm';
import ChangePasswordForm from '../components/Auth/ChangePasswordForm';
import colors from "../styles/colors";

const Stack = createStackNavigator();

export default function LoginStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerTintColor: colors.fontLight,
          headerStyle: { backgroundColor: colors.bgDark },
          cardStyle: {
            backgroundColor: colors.bgLight,
          },
        }}
      >
        <Stack.Screen
          name="login"
          component={LoginForm}
          options={{ headerShown: false }}
        />
         <Stack.Screen
          name="changePassword"
          component={ChangePasswordForm}
          options={{ headerShown: false }}
        />
  
  
      
      </Stack.Navigator>
    );
  }
  
