import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import colors from "../styles/colors";
import Cart from "../screens/Cart";
import DeleteProduct from "../screens/Products/DeleteProduct";
import Carrito from "../screens/Product/Carrito";
import Carrodecompras from "../screens/Account/Carrodecompras";
import Carrodecompras2 from "../screens/Account/Carrodecompras2";
import Carrodecompras3 from "../screens/Account/Carrodecompras3";
import Carrodecompras4 from "../screens/Account/Carrodecompras4";
import SearchProduct from "../components/Products/Route3";
const Stack = createStackNavigator();

export default function CartStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTintColor: colors.fontLight,
        headerStyle: { backgroundColor: colors.bgDark },
        cardStyle: {
          backgroundColor: colors.bgLight,
        },
      }}
    >
      <Stack.Screen
        name="Cart"
        component={Cart}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Carrito"
        component={Carrito}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="DeleteProduct"
        component={DeleteProduct}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Carrodecompras"
        component={Carrodecompras}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Carrodecompras2"
        component={Carrodecompras2}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Carrodecompras3"
        component={Carrodecompras3}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Carrodecompras4"
        component={Carrodecompras4}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SearchProduct"
        component={SearchProduct}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
