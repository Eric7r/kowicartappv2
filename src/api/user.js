import {API_URL} from "../utils/constanst";

export async function loginApi(formData) {
    try {
        const url = `${API_URL}/api/login`;
        const params = {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify(formData),
          };
          const response = await fetch(url, params);
          const result = await response.json();
          return result;
    } catch (error) {
        console.log(error);
    return error;
    }
}



export async function getUserApi(id) {
  try {
    const url = `${API_URL}/api/userDetail/${id}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function deleteUserApp(formData) {
  try {
      console.log("id:"+JSON.stringify(formData));
      const url = `${API_URL}/api/eliminarUsuarioApp`;
      const params = {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(formData),
        };
        const response = await fetch(url, params);
        const result = await response;
        //console.log("result"+result.status);
        return result.status;
  } catch (error) {
      console.log("ERROR CATCH"+error);
  return error;
  }
}

