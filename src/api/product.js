import { stringify } from "querystring";
import { API_URL } from "../utils/constanst";

export async function getLastProductApi() {
  try {
    const url = `${API_URL}/products1/${"1324"}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function getProductApi(idproduct, idusuario) {
  try {
    const url = `${API_URL}/api/productselecteds/${idproduct}/${idusuario}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function getProductExistenciaApi(id) {
  try {
    const url = `${API_URL}/api/products/${id}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function searchProductsApi(search,iduser) {
  try {
    const url = `${API_URL}/api/buscar/${search}/${iduser}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function getCartId(idUsuario) {
  try {
    const url = `${API_URL}/api/cartInfo/${idUsuario}`;
    const response = await fetch(url);
    const result = await response.text();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function getCart(id) {
  try {
    const url = `${API_URL}/api/homePrueba/${id}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function updateCarrito(formData) {
  try {
    const url = `${API_URL}/api/updateCarrito`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}
export async function agregarProducto(formData) {
  try {
    const url = `${API_URL}/api/storeprueba`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function agregaruser(formData) {
  try {
    const url = `${API_URL}/api/registeruser`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return error;
  }
}

export async function getUserEditApi(idusuario) {
  try {
    const url = `${API_URL}/api/userSetting/${idusuario}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function updateuser(formData) {
  try {
    const url = `${API_URL}/api/updateUser`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function updateuser2(formData) {
  try {
    const url = `${API_URL}/api/updateUser2`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function updateuser3(formData) {
  try {
    const url = `${API_URL}/api/updateUser3`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function updateUser4(formData) {
  try {
    const url = `${API_URL}/api/updateUser4`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function reporterror(formData) {
  try {
    const url = `${API_URL}/api/ReportError`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function forgot_pass(formData) {
  try {
    const url = `${API_URL}/api/forgot-password`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function getProductDelete(id_cart_details) {
  try {
    const url = `${API_URL}/api/deleteEdit/${id_cart_details}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function getColoniasByCity(city) {
  try {
    const url = `${API_URL}/api/coloniaByCity/${city}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function deleteDetalle(formData) {
  try {
    const url = `${API_URL}/api/deleteDetalle`;
    const params = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(formData),
    };
    const response = await fetch(url, params);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function getHistoryApi(idusuario) {
  try {
    const url = `${API_URL}/api/historialpedidos/${idusuario}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
  } catch (error) {
    console.log(error);
    return null;
  }
}