export const API_URL = "https://apprutas.kowi.com.mx";
export const API_URL_IMAGES ="https://apprutas.kowi.com.mx/images/products/";
export const TOKEN = "token";
export const IDUSER = "iduser";
export const SEARCH_HISTORY = "search-history";
export const CART = "cart";