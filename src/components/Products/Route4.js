import React, {useState,useEffect,useMemo} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import IndexRoute4 from "./IndexRoute4";
import jwtDecode from "jwt-decode";
import {getTokenApi} from "../../api/token";
import { API_URL } from "../../utils/constanst";
import { getCart } from "../../api/product";
import { useIsFocused } from '@react-navigation/native';
export default function Route4() {

    const [Index1, setIndex1] = useState(null);
    const [idUser, setIdUser] = useState(undefined);
    const isFocused = useIsFocused();

    useEffect(() => {
        (async () => {
          const token = await getTokenApi();
      
          if (token) {
      
            setIdUser({
              userid:jwtDecode(token).sub,
           
            });
          } else {
            setIdUser(null);
          }
          
          const response = await getCart(jwtDecode(token).sub);
          setIndex1(response);
              
        })();
      }, [isFocused]);
     
    
    
    return (
        <View>
            {Index1 && <IndexRoute4 Index1={Index1} />}
        </View>
    )
}


const styles = StyleSheet.create({

    container: {
        padding: 10,
        marginTop: 20,
    },

    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 10,
    },

});