import React, {useState,useEffect,useMemo} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Carrito from "./Carrito";
import jwtDecode from "jwt-decode";
import {getTokenApi} from "../../api/token";
import { API_URL } from "../../utils/constanst";
import { useIsFocused } from '@react-navigation/native';
export default function IndexRoute4(props) {
    const { Index1 } = props;
    const [Index2, setIndex2] = useState(null);
    const [Index3, setIndex3] = useState(null);
    const [idUser, setIdUser] = useState(undefined);
    const isFocused = useIsFocused();

    useEffect(() => {
      (async () => {
        const token = await getTokenApi();
    
        if (token) {
    
          setIdUser({
            userid:jwtDecode(token).sub,
         
          });
        } else {
          setIdUser(null);
        }
         const url = `${API_URL}/api/homes2/${jwtDecode(token).sub}`
    
        fetch(url).then((response) => response.json()).then((responseJson) => {
          let dataSource = [];
    
          Object.values(responseJson).forEach(item => {
              dataSource = dataSource.concat(item);
          });
    
          setIndex3(dataSource);
          
        
      });      
      })();
    }, [isFocused]);

    useEffect(() => {
        (async () => {
          const token = await getTokenApi();
      
          if (token) {
      
            setIdUser({
              userid:jwtDecode(token).sub,
           
            });
          } else {
            setIdUser(null);
          }
           const url = `${API_URL}/api/homesErzu/${jwtDecode(token).sub}`
      
          fetch(url).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
      
            setIndex2(dataSource);
            
          
        });      
        })();
      }, [isFocused]);
     
    
    return (
        <View>
           {Index2,Index1,Index3 && <Carrito Index2={Index2} Index1={Index1} Index3={Index3} />}
        </View>
    )
}


const styles = StyleSheet.create({

    container: {
        padding: 10,
        marginTop: 20,
    },

    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 10,
    },

});