import React, { useState, useEffect } from "react";
import {
  Image,
  TouchableHighlight,
  Text,
  Modal,
  View,
  Button,
  ScrollView,
  StyleSheet,
  CheckBox,
  TextInput,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
  ToastAndroid,
} from "react-native";
import Icon from "@expo/vector-icons/AntDesign";
import Icon2 from "@expo/vector-icons/Feather";
import Icon3 from "@expo/vector-icons/FontAwesome";
import Icon4 from "@expo/vector-icons/Entypo";
import Icon5 from "@expo/vector-icons/Octicons";
import { map } from "lodash";
import { useNavigation } from "@react-navigation/native";
import { API_URL_IMAGES } from "../../utils/constanst";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { RootSiblingParent } from "react-native-root-siblings";
import { deleteDetalle } from "../../api/product";
import { stringify } from "querystring";

export default function IndexRoute3(props) {
  const { Index1 } = props;
  const { route } = props;
  console.log("DETALLEPROD:"+JSON.stringify(route.params.itemProducto));
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);

  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await deleteDetalle(formData);
        if (Platform.OS === 'android') {
          ToastAndroid.show("Producto Borrado", ToastAndroid.SHORT)
        } else {
          Toast.show("Producto Borrado", { position: Toast.positions.CENTER, });
        }
      } catch (error) {
        setLoading(false);
        if (Platform.OS === 'android') {
          ToastAndroid.show("Verifique sus datos", ToastAndroid.SHORT)
        } else {
          Toast.show("Verifique sus datos", { position: Toast.positions.CENTER, });
        }
      }
    },
  });

  return (
    
    <ScrollView style={styles.modal}>
        <View>
            <View
              style={{ width: "100%" }}>
              <Text
                style={{
                  fontSize: 23,
                  alignSelf: "center",
                  color: "#808080",
                  marginTop: 20,
                }}>
                ¿Realmente quiere eliminar {route.params.itemProducto} de su carrito?
              </Text>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  marginTop: 0,
                  paddingVertical: 2,
                  width: "100%",
                }}>
                <TextInput
                  onChangeText={(text) => formik.setFieldValue("cart_detail_id", text)}
                  value={(formik.values.cart_detail_id = route.params.paramKey3.toString())}
                  error={formik.errors.cart_detail_id}
                  style={{
                    borderWidth: 0.5,
                    borderColor: "#808080",
                    width: "20%",
                    marginTop: -20,
                    color: "#808080",
                    textAlign: "center",
                    marginBottom: 40,
                    display: "none",
                  }}
                />
                <View
                  style={{
                    alignItems: "center",
                    alignSelf: "center",
                    justifyContent: "center",
                    marginTop: 30,
                    backgroundColor: "#fff",
                    paddingVertical: 0,
                    borderRadius: 23,
                    width: "40%",
                    margin: 20
                  }}
                >
                  <TouchableOpacity
                    mode="contained"
                    onPress={
                      formik.handleSubmit
                    }
                    loading={setLoading}
                    title="Press Me"
                    onPressIn={() =>
                      navigation.navigate(
                        "Carrodecompras2"
                      )
                    }
                    style={{
                      backgroundColor:
                        "#FF7709",
                      borderRadius: 23,
                      height: 45,
                      marginBottom: 0,
                      width: "100%"
                    }}
                  >
                    <Text
                      style={{
                        fontSize: 24,
                        color: "#fff",
                        alignSelf: "center",
                        paddingTop: 5,
                        fontWeight: "600",
                      }}
                    >
                      Si
                    </Text>
                  </TouchableOpacity>
                </View>
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    marginTop: 30,
                    backgroundColor: "#fff",
                    paddingVertical: 0,
                    borderRadius: 23,
                    width: "40%",
                    margin: 20
                  }}>
                  <TouchableOpacity
                    mode="contained"
                    onPress={() => navigation.navigate("Carrodecompras2")}
                    style={{
                      backgroundColor:
                        "#FF7709",
                      borderRadius: 23,
                      height: 45,
                      marginBottom: 0,
                      width: "100%"
                    }}>
                    <Text
                      style={{
                        fontSize: 24,
                        color: "#fff",
                        alignSelf: "center",
                        paddingTop: 5,
                        fontWeight: "600",
                      }}
                    >
                      No
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
        </View>
    </ScrollView>
  );
}

function initialValues() {
  return {
    cart_detail_id: "",
  };
}

function validationSchema() {
  return {
    cart_detail_id: Yup.string().required(true),
  };
}

const styles = StyleSheet.create({
  
 
  modal:{
    backgroundColor: "white",
    padding: 10,
    marginTop:'30%',
    borderRadius: 30,
    shadowColor: "black",
    shadowOffset: {
        width: 0,
        height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 10,
  }
});
