import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import IndexRoute3 from "./IndexRoute3";
import { API_URL_IMAGES } from "../../utils/constanst";
import { API_URL } from "../../utils/constanst"
import jwtDecode from "jwt-decode";
import {getTokenApi} from "../../api/token";

export default function Route3(props) {
    const { route } = props;
    const { params } = route;
    const [Index1, setIndex1] = useState(null);
    const [idUser, setIdUser] = useState(undefined);

    useEffect(() => {
        (async () => {
          const token = await getTokenApi();
      
          if (token) {
      
            setIdUser({
              userid:jwtDecode(token).sub,
           
            });
          } else {
            setIdUser(null);
          }
           const url = `${API_URL}/api/deleteEdit/${route.params.paramKey3}`
      
          fetch(url).then((response) => response.json()).then((responseJson) => {
            let dataSource = [];
      
            Object.values(responseJson).forEach(item => {
                dataSource = dataSource.concat(item);
            });
      
            setIndex1(dataSource);
            
          
        });      
        })();
      }, []);
    return (
        <View style={styles.container}>
            {Index1,route && <IndexRoute3 Index1={Index1} route={route} />}

        </View>
    )
}


const styles = StyleSheet.create({

    container: {
        padding: 10,
        marginTop: 20,
    },

    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 10,
    },

});