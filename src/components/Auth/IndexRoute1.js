import React, { useState, useCallback, useEffect } from "react";
import {
  Picker,
  ScrollView,
  View,
  Text,
  StyleSheet,
  SafeAreaView,
} from "react-native";
import Icon from "@expo/vector-icons/AntDesign";
import { TextInput, Button } from "react-native-paper";
import { CheckBox } from "react-native-elements";
import { formStyle } from "../../styles";
import { agregaruser } from "../../api/product";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { map } from "lodash";
import { API_URL } from "../../utils/constanst";
import { useFocusEffect } from "@react-navigation/native";
import {getColoniasByCity} from "../../api/product";

export default function IndexRoute1(props) {
  const { changeForm } = props;
  const { Index1 } = props;
  const { Index2 } = props;
  const [loading, setLoading] = useState(false);
  const [selectedValue, setSelectedValue] = useState("20");
  const [selectedValue2, setSelectedValue2] = useState("20");
  const [colonias, setlstColonias] = useState(null);
  
  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      
      try {
        setLoading(true);
        const response = await agregaruser(formData);
        if(response.statusCode=="200"){
          alert(response.respuesta);
          console.log(response);
          changeForm(); 
        }else{
          alert(response.respuesta);
          console.log(response);
        }
       
      } catch (error) {
        setLoading(false);
        
      }
    },
  });

  //Inicio
  useEffect(() => {
    (async () =>{

    const url = `${API_URL}/api/coloniaByCity/`+selectedValue2;
    console.log(selectedValue2);
    fetch(url)
    .then((response) => response.json())
    .then((responseJson) => {
      let dataSource = [];
      Object.values(responseJson).forEach((item) => {
        dataSource = dataSource.concat(item);
      });
      //Setea el valor de colonias
      //setSelectedValue(dataSource);
      setlstColonias(dataSource);
      console.log(colonias);
    });

    })()
}, [selectedValue2])
  //Fin

  function changeCity(value){
    //Setea el valor de ciudad
    setSelectedValue2(value);

    const url = `${API_URL}/api/coloniaByCity/`+value;
    
    fetch(url)
    .then((response) => response.json())
    .then((responseJson) => {
      let dataSource = [];
      Object.values(responseJson).forEach((item) => {
        dataSource = dataSource.concat(item);
      });
      //Setea el valor de colonias falta que haga el refresh
      setSelectedValue(dataSource);
      console.log(dataSource);
    });
        
  }

 
  return (
    <SafeAreaView>
      <ScrollView>
        <View style={{ height: "auto", marginTop: 20, marginBottom: 40 }}>
          <Text
            style={{
              fontSize: 25,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "600",
              marginTop: 5,
              marginBottom: 0,
            }}
          >
            Regístrate
          </Text>
          <Text
            style={{
              fontSize: 12,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "100",
              marginTop: 0,
              marginBottom: 20,
            }}
          >
            ingresa tus datos
          </Text>

          <TextInput
            onChangeText={(text) => formik.setFieldValue("name", text)}
            value={formik.values.name}
            error={formik.errors.name}
            label="Nombre"
            style={formStyle.input}
          />
          <TextInput
            onChangeText={(text) => formik.setFieldValue("username", text)}
            value={formik.values.username}
            error={formik.errors.username}
            label="Nombre de usuario"
            style={formStyle.input}
          />
          <TextInput
            onChangeText={(text) => formik.setFieldValue("email", text)}
            value={formik.values.email}
            error={formik.errors.email}
            label="Correo electrònico"
            style={formStyle.input}
          />
          <TextInput
            onChangeText={(text) => formik.setFieldValue("phone", text)}
            value={formik.values.phone}
            error={formik.errors.phone}
            keyboardType='numeric'
            label="Telefono"
            style={formStyle.input}
          />
          <Picker
            selectedValue={selectedValue2}
            style={{ height: 50, width: 150 }}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValue2(itemValue)
              //changeCity(itemValue)
            }
            value={(formik.values.city = selectedValue2)}
            error={formik.errors.city}
            style={{ width: "100%", color: "#4C4C4C", marginTop: -10 }}
          >
            <Picker.Item label="Ciudad" value="20" />
            {map(Index2, (Indexs2) => (
              <Picker.Item
                label={Indexs2.name}
                value={Indexs2.name}
                key={Indexs2}
              />
            ))}
          </Picker>
          
          <TextInput
            onChangeText={(text) => formik.setFieldValue("codigopostal", text)}
            value={formik.values.codigopostal}
            error={formik.errors.codigopostal}
            keyboardType='numeric'
            label="Codigo postal"
            style={formStyle.input}
          />
          <Picker
            selectedValue={selectedValue}
            style={{ height: 50, width: 150 }}
            onValueChange={(itemValue, itemIndex) =>
              setSelectedValue(itemValue)
            }
            value={(formik.values.colonia = selectedValue)}
            error={formik.errors.colonia}
            style={{ width: "100%", color: "#4C4C4C", marginTop: -10 }}
          >
            <Picker.Item label="Colonia" value="20" />
            {map(colonias, (item) => (
              <Picker.Item
                label={item.colonia}
                value={item.colonia}
                key={item}
              />
            ))}
          </Picker>
          
       

          <TextInput
            onChangeText={(text) => formik.setFieldValue("address", text)}
            value={formik.values.address}
            error={formik.errors.address}
            label="Calle"
            style={formStyle.input}
          />
          <TextInput
            onChangeText={(text) =>
              formik.setFieldValue("numeroexterior", text)
            }
            value={formik.values.numeroexterior}
            error={formik.errors.numeroexterior}
            label="Numero Exterior"
            style={formStyle.input}
          />
          <TextInput
            onChangeText={(text) =>
              formik.setFieldValue("numerointerior", text)
            }
            value={formik.values.numerointerior}
            error={formik.errors.numerointerior}
            label="Numero Interior (opcional)"
            style={formStyle.input}
          />
          <TextInput
            onChangeText={(text) => formik.setFieldValue("rfc", text)}
            value={formik.values.rfc}
            error={formik.errors.rfc}
            label="RFC (opcional)"
            style={formStyle.input}
          />
          <TextInput
            onChangeText={(text) => formik.setFieldValue("password", text)}
            value={formik.values.password}
            error={formik.errors.password}
            label="Contraseña"
            style={formStyle.input}
            secureTextEntry
          />
          <TextInput
            label="Repetir Contraseña"
            style={formStyle.input}
            secureTextEntry
          />
          <CheckBox
            checkedIcon="check-box"
            iconType="material"
            uncheckedIcon="check-box-outline-blank"
            title="Estoy de acuerdo con la Politica de Privacidad"
            checkedTitle="He leído y estoy de acuerdo con la Politica de Privacidad"
            checked={formik.values.privacidad}
            onPress={() =>
              formik.setFieldValue("privacidad", !formik.values.privacidad)
            }
          />

          {(() => {
            if (!formik.values.name) {
              return (
                <Button
                  mode="contained"
                  onPress={formik.handleSubmit}
                  loading={setLoading}
                  onPressIn={changeForm}
                  color="#FF7709"
                  style={{ height: 40, borderRadius:20 }}
                  disabled={true}
                >
                  Registrarse
                </Button>
              );
            }
            return (
              <View>
                {(() => {
                  if (!formik.values.username) {
                    return (
                      <View>
                        {/* Iniciar sesion */}
                        <Button
                          mode="contained"
                          onPress={formik.handleSubmit}
                          loading={setLoading}
                          onPressIn={changeForm}
                          color="#FF7709"
                          style={{ height: 40, borderRadius:20  }}
                          disabled={true}
                        >
                          Registrarse
                        </Button>
                      </View>
                    );
                  }
                  return (
                    <View>
                      {(() => {
                        if (!formik.values.email) {
                          return (
                            <View>
                              {/* Iniciar sesion */}

                              <Button
                                mode="contained"
                                onPress={formik.handleSubmit}
                                loading={setLoading}
                                onPressIn={changeForm}
                                color="#FF7709"
                                style={{ height: 40, borderRadius:20  }}
                                disabled={true}
                              >
                                Registrarse
                              </Button>
                            </View>
                          );
                        }
                        return (
                          <View>
                            {(() => {
                              if (!formik.values.phone) {
                                return (
                                  <View>
                                    {/* Iniciar sesion */}

                                    <Button
                                      mode="contained"
                                      onPress={formik.handleSubmit}
                                      loading={setLoading}
                                      onPressIn={changeForm}
                                      color="#FF7709"
                                      style={{ height: 40, borderRadius:20  }}
                                      disabled={true}
                                    >
                                      Registrarse
                                    </Button>
                                  </View>
                                );
                              }
                              return (
                                <View>
                                  {(() => {
                                    if (formik.values.city == "20") {
                                      return (
                                        <View>
                                          {/* Iniciar sesion */}

                                          <Button
                                            mode="contained"
                                            onPress={formik.handleSubmit}
                                            loading={setLoading}
                                            onPressIn={changeForm}
                                            color="#FF7709"
                                            style={{ height: 40, borderRadius:20  }}
                                            disabled={true}
                                          >
                                            Registrarse
                                          </Button>
                                        </View>
                                      );
                                    }
                                    return (
                                      <View>
                                        {(() => {
                                          if (!formik.values.codigopostal) {
                                            return (
                                              <View>
                                                {/* Iniciar sesion */}

                                                <Button
                                                  mode="contained"
                                                  onPress={formik.handleSubmit}
                                                  loading={setLoading}
                                                  onPressIn={changeForm}
                                                  color="#FF7709"
                                                  style={{ height: 40, borderRadius:20  }}
                                                  disabled={true}
                                                >
                                                  Registrarse
                                                </Button>
                                              </View>
                                            );
                                          }
                                          return (
                                            <View>
                                              {(() => {
                                                if (
                                                  formik.values.colonia == "20"
                                                ) {
                                                  return (
                                                    <View>
                                                      {/* Iniciar sesion */}

                                                      <Button
                                                        mode="contained"
                                                        onPress={
                                                          formik.handleSubmit
                                                        }
                                                        loading={setLoading}
                                                        onPressIn={changeForm}
                                                        color="#FF7709"
                                                        style={{ height: 40, borderRadius:20  }}
                                                        disabled={true}
                                                      >
                                                        Registrarse
                                                      </Button>
                                                    </View>
                                                  );
                                                }
                                                return (
                                                  <View>
                                                    {(() => {
                                                      if (
                                                        !formik.values.address
                                                      ) {
                                                        return (
                                                          <View>
                                                            {/* Iniciar sesion */}

                                                            <Button
                                                              mode="contained"
                                                              onPress={
                                                                formik.handleSubmit
                                                              }
                                                              loading={
                                                                setLoading
                                                              }
                                                              onPressIn={
                                                                changeForm
                                                              }
                                                              color="#FF7709"
                                                              style={{
                                                                height: 40, borderRadius:20 ,
                                                              }}
                                                              disabled={true}
                                                            >
                                                              Registrarse
                                                            </Button>
                                                          </View>
                                                        );
                                                      }
                                                      return (
                                                        <View>
                                                          {(() => {
                                                            if (
                                                              !formik.values
                                                                .numeroexterior
                                                            ) {
                                                              return (
                                                                <View>
                                                                  {/* Iniciar sesion */}

                                                                  <Button
                                                                    mode="contained"
                                                                    onPress={
                                                                      formik.handleSubmit
                                                                    }
                                                                    loading={
                                                                      setLoading
                                                                    }
                                                                    onPressIn={
                                                                      changeForm
                                                                    }
                                                                    color="#FF7709"
                                                                    style={{
                                                                      height: 40, borderRadius:20 ,
                                                                    }}
                                                                    disabled={
                                                                      true
                                                                    }
                                                                  >
                                                                    Registrarse
                                                                  </Button>
                                                                </View>
                                                              );
                                                            }
                                                            return (
                                                              <View>
                                                                {(() => {
                                                                  if (
                                                                    !formik
                                                                      .values
                                                                      .password
                                                                  ) {
                                                                    return (
                                                                      <View>
                                                                        {/* Iniciar sesion */}

                                                                        <Button
                                                                          mode="contained"
                                                                          onPress={
                                                                            formik.handleSubmit
                                                                          }
                                                                          loading={
                                                                            setLoading
                                                                          }
                                                                          onPressIn={
                                                                            changeForm
                                                                          }
                                                                          color="#FF7709"
                                                                          style={{
                                                                            height: 40, borderRadius:20 ,
                                                                          }}
                                                                          disabled={
                                                                            true
                                                                          }
                                                                        >
                                                                          Registrarse
                                                                        </Button>
                                                                      </View>
                                                                    );
                                                                  }
                                                                  return (
                                                                    <View>
                                                                      <Button
                                                                        mode="contained"
                                                                        onPress={
                                                                          formik.handleSubmit
                                                                        }
                                                                        onPressIn={
                                                                          changeForm
                                                                        }
                                                                        color="#FF7709"
                                                                        style={{
                                                                          height: 40, borderRadius:20 ,
                                                                        }}
                                                                      >
                                                                        <Text
                                                                          style={{
                                                                            color:
                                                                              "#fff",
                                                                          }}
                                                                        >
                                                                          Registrarse
                                                                        </Text>
                                                                      </Button>
                                                                    </View>
                                                                  );
                                                                })()}
                                                              </View>
                                                            );
                                                          })()}
                                                        </View>
                                                      );
                                                    })()}
                                                  </View>
                                                );
                                              })()}
                                            </View>
                                          );
                                        })()}
                                      </View>
                                    );
                                  })()}
                                </View>
                              );
                            })()}
                          </View>
                        );
                      })()}
                    </View>
                  );
                })()}
              </View>
            );
          })()}

          <Button
            mode="text"
            style={formStyle.btnText}
            labelStyle={formStyle.btnTextLabel}
            onPress={changeForm}
          >
            Iniciar Sesión
          </Button>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
function initialValues() {
  return {
    name: "",
    username: "",
    email: "",
    phone: "",
    city: "",
    codigopostal: "",
    colonia: "",
    address: "",
    numeroexterior: "",
    numerointerior: "",
    privacidad: false,
    rfc: "",
    password: "",
  };
}

function validationSchema() {
  return {
    name: Yup.string().required(true),
    username: Yup.string().required(true),
    email: Yup.string().required(true),
    phone: Yup.string().required(true),
    city: Yup.string().required(true),
    codigopostal: Yup.string().required(true),
    colonia: Yup.string().required(true),
    address: Yup.string().required(true),
    privacidad: Yup.boolean().oneOf([true], "Please check the agreement"),
    numeroexterior: Yup.string().required(true),
    password: Yup.string().required(true),
  };
}

const styles = StyleSheet.create({
  searchSection: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    backgroundColor: "#fff",
    color: "#424242",
  },
});
