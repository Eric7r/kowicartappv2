import React, { useState, useEffect } from "react";
import {
  Picker,
  Image,
  TouchableHighlight,
  Text,
  Modal,
  View,
  ScrollView,
  StyleSheet,
  CheckBox,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from "react-native";
import Icon from "@expo/vector-icons/AntDesign";
import { TextInput, Button } from "react-native-paper";
import { formStyle } from "../../../styles";
import { forgot_pass } from "../../../api/product";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";
import { map } from "lodash";
import { useNavigation } from "@react-navigation/native";

export default function IndexRoute7(props) {
  const { Index1 } = props;
  const navigation = useNavigation();

  const [loading, setLoading] = useState(false);
  const formik = useFormik({
    initialValues: initialValues(),
    validationSchema: Yup.object(validationSchema()),
    onSubmit: async (formData) => {
      setLoading(true);
      try {
        const response = await forgot_pass(formData);
        Toast.show("Datos guardados", {
          position: Toast.positions.CENTER,
        });
      } catch (error) {
        Toast.show("Verifique sus datos", {
          position: Toast.positions.CENTER,
        });
        setLoading(false);
      }
    },
  });

  return (
    <SafeAreaView>
      <ScrollView>
        <View style={{ height: "auto", marginTop: 20, marginBottom: 40 }}>
          <Text
            style={{
              fontSize: 25,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "600",
              marginTop: 5,
              marginBottom: 0,
            }}
          >
           Tus compras
          </Text>
          <Text
            style={{
              fontSize: 12,
              alignSelf: "center",
              color: "#808080",
              fontWeight: "100",
              marginTop: 0,
              marginBottom: 20,
            }}
          >
            Pedidos realizados
          </Text>
     
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              marginHorizontal: 15,
              alignSelf:'center',
              marginTop: 15,
              paddingHorizontal: 10,
              paddingVertical: 2,
              backgroundColor: "#fff",
              borderBottomWidth: 1,
              borderBottomColor: "#D8D8D8",
              width:"100%"
            }}
          >
            {/* TH */}
            <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>
              Pedido
            </Text>
            <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>
              fecha
            </Text>
            <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>
              importe
            </Text>
            <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>
              Estatus
            </Text>
          </View>
          {map(Index1, (Indexs1) => (
              <React.Fragment key={Indexs1.id}>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginHorizontal: 10,
                    alignSelf:'center',
                    borderBottomWidth: 1,
                    borderBottomColor: "#D8D8D8",                   
                    paddingVertical: 2,
                    backgroundColor: "#fff",
                  }}>

                   <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>#{Indexs1.id}</Text>
                   <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>{Indexs1.order_date}</Text>
                   <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>${Indexs1.importe_total}</Text>
                   <Text style={{ width: "25%", fontSize: 10, color: "#333333" }}>{Indexs1.status}</Text>
                </View>
              </React.Fragment>
            ))}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

function initialValues() {
  return {
    email: "",
  };
}

function validationSchema() {
  return {
    email: Yup.string().required(true),
    
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    margin: -3,
  },
  buttonagregar: {
    backgroundColor: "#FF7709",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 10,
  },
  buttonagregar2: {
    backgroundColor: "#C1C1C1",
    borderRadius: 23,
    width: "100%",
    height: 40,
    marginBottom: 10,
  },
  postInput: {
    fontSize: 16,
    borderColor: "#42435b",
    borderWidth: 1,
    margin: 10,
    fontFamily: "Outrun future",
  },
});
