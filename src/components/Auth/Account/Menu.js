import React, { useEffect, useState } from "react";
import { Alert, ToastAndroid } from "react-native";
import { List, TextInput } from "react-native-paper";
import {  useNavigation } from "@react-navigation/native";
import useAuth from "../../../hooks/useAuth";
import { getTokenApi} from "../../../api/token";
import jwtDecode from "jwt-decode";
import { deleteUserApp } from "../../../api/user";
import { useFormik } from "formik";
import * as Yup from "yup";
import Toast from "react-native-root-toast";

export default function Menu() {
  
  const navigation = useNavigation();
  const { logout } = useAuth();
  const [auth, setAuth] = useState(true);
  const [idUsuario, setIdUsuario] = useState(0);
  const [loading, setLoading] = useState(false);

  {/*Obtiene el id del usuario logeado*/}
  useEffect(() => {
    (async () => {
      const token = await getTokenApi();
      if (token) {
        setIdUsuario(jwtDecode(token).sub,);
      } else {
        setIdUsuario(null);
      }  
    })();
  },[]);

  {/*Cacha el auth para cuando elimines el usuario te mande al login*/}
  useEffect(() => {
    (async () => {
      if (auth) {
      } else {
        logout();
      }
    })();
  }, [auth]);
 
   {/*Cerrar sesion*/}
  const logoutAccount = () => {
    Alert.alert(
      "Cerrar sesión",
      "¿Estas seguro de que quieres salir de tu cuenta?",
      [
        {
          text: "NO",
        },
        { text: "SI", onPress: logout },
      ],
      { cancelable: true }
    );
  };

  {/*Elimina usuario de la app foreva*/}
  const deleteUser = () => {
    Alert.alert(
      "Eliminar usuario",
      "¿Estas seguro de que quieres eliminar cuenta?",
      [
        {
          text: "NO",
        },
        { text: "SI", onPress: confirmationDelete() },
      ],
      { cancelable: true }
    );
  };

   {/*Confirmacion modal delete usuario*/}
  const confirmationDelete = () => {
    Alert.alert(
      "Su cuenta sera eliminada permanentemente",
      "¿Esta seguro de realizar esta acción?",
      [
        {
          text: "NO",
        },
        { text: "SI", onPress: formik.handleSubmit },
      ],
      { cancelable: true }
    );
  };


{/**Define el objeto a mandar en la peticion */}
  const initialValues=()=> {
    return {userid:idUsuario};
  }

{/**Tipos de datos del objeto a mandar */}
function validationSchema() {
  return {
    userid: Yup.number().required(true),
  };
}

{/*Activa en false el auth para el cierre de sesion*/ }
const cerrarSesion=()=>{
   setAuth(false)
} 

 {/**Servicio que hace un update al eliminado y username del usuario */}
 const formik = useFormik({
  initialValues: initialValues(),
  validationSchema: Yup.object(validationSchema()),
  onSubmit: async (formData) => {
    setLoading(true);
    try {
      {}
      const response = await deleteUserApp(formData);
      if (response===200){
        Platform.OS === 'android'
          ?ToastAndroid.show("Usuario eliminado correctamente", ToastAndroid.SHORT)
          :Toast.show("Usuario eliminado correctamente", { position: Toast.positions.CENTER, })
          cerrarSesion();
      }else{
       Platform.OS === 'android'
          ?ToastAndroid.show("ERROR:"+response, ToastAndroid.SHORT)
          :Toast.show("Error not success", { position: Toast.positions.CENTER, });
      }   
    } catch (error) {
      setLoading(false);
      Platform.OS === 'android'
        ?ToastAndroid.show("Error! El usuario no se pudo eliminar", ToastAndroid.SHORT)
        :Toast.show("Error usuario no se pudo eliminar", { position: Toast.positions.CENTER, });
    }
  }
});

  return (
    <>
      <List.Section>
        <List.Subheader  style={{color:"#ff6900", fontSize:17 , marginTop:10}}>Mi cuenta</List.Subheader>
        <List.Item
          title="Cambiar nombre"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cambia el nombre de tu cuenta"
          left={(props) => <List.Icon {...props} icon="face" />}
          onPress={() => navigation.navigate("EditUser")}
        />
        <List.Item
          title="Cambiar email"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cambia el email de tu cuenta"
          left={(props) => <List.Icon {...props} icon="at" />}
          onPress={() => navigation.navigate("EditUser2")}
        />
        <List.Item
          title="Cambiar username"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cambia el nombre de usuario de tu cuenta"
          left={(props) => <List.Icon {...props} icon="sim" />}
          onPress={() => navigation.navigate("EditUser3")}
        />
        <List.Item
          title="Cambiar contraseña"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cambia el contraseña de tu cuenta"
          left={(props) => <List.Icon {...props} icon="key" />}
          onPress={() => navigation.navigate("EditUser5")}
        />
        <List.Item
          title="Mis direcciones"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Administra tus direcciones de envio"
          left={(props) => <List.Icon {...props} icon="map" />}
          onPress={() => navigation.navigate("EditUser4")}
        />
        <List.Item
          title="Eliminar cuenta"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Elimina tu cuenta"
          left={(props) => <List.Icon {...props} icon="delete" />}
          onPress={deleteUser}
          
        />
        {/*Toma el valor de idusuario*/}
        <TextInput
            onChangeText={(text) => formik.setFieldValue("userid", text)}
            value={(formik.values.userid = idUsuario)}
            error={formik.errors.userid}
            label="iduser"
            style={{ display: "none" }}
          />
      </List.Section>

      <List.Section>
        <List.Subheader style={{color:"#ff6900", fontSize:17 , marginTop:10}}>App</List.Subheader>
        <List.Item
          title="Pedidos"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Listado de todos los pedidos"
          left={(props) => <List.Icon {...props} icon="clipboard-list" />}
          onPress={() => navigation.navigate("EditUser7")}
        />

        <List.Item
          title="Reportar Error"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Reporta cualquier error que se te pueda presentar"
          left={(props) => <List.Icon {...props} icon="alert" />}
          onPress={() => navigation.navigate("ReportError")}
        />

        <List.Item
          title="Cerrar sesión"
          titleStyle={{color: "rgba(0,0,0,0.8)", fontSize:17, fontWeight:'bold'}}
          description="Cierra esta sesion y inicia con otra"
          left={(props) => <List.Icon {...props} icon="logout" />}
          onPress={logoutAccount}
        />
      </List.Section>
    </>
  );
}
