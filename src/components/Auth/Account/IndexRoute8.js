import React, { useState, useEffect } from "react";
import {
  Text,
  View,
  ScrollView,
  SafeAreaView,
} from "react-native";

import { useNavigation } from "@react-navigation/native";

export default function IndexRoute8() {
  const navigation = useNavigation();
  const [loading, setLoading] = useState(false);
 

  return (
    <SafeAreaView>
      <ScrollView>
        <View>
          <Text style={{ fontSize: 20,alignSelf: "center",color: "#808080",fontWeight: "600", }}>Tus Mensajes</Text>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

