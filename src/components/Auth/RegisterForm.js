import React, { useState, useEffect } from "react";
import { View, StyleSheet } from "react-native";
import IndexRoute1 from "./IndexRoute1";
import { API_URL } from "../../utils/constanst";

export default function RegisterForm(props) {
  const { changeForm } = props;
  const [Index1, setIndex1] = useState(null);
  const [Index2, setIndex2] = useState(null);

  const url = `${API_URL}/api/registerprueba`;

  useEffect(() => {
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        let dataSource = [];

        Object.values(responseJson).forEach((item) => {
          dataSource = dataSource.concat(item);
        });

        setIndex1(dataSource);
      });
  }, []);
  const url2 = `${API_URL}/api/registerprueba2`;

  useEffect(() => {
    fetch(url2)
      .then((response) => response.json())
      .then((responseJson) => {
        let dataSource = [];

        Object.values(responseJson).forEach((item) => {
          dataSource = dataSource.concat(item);
        });

        setIndex2(dataSource);
      });
  }, []);
  return (
    <View style={styles.container}>
      {
        (changeForm,
        Index1,
        Index2 && (
          <IndexRoute1
            changeForm={changeForm}
            Index1={Index1}
            Index2={Index2}
          />
        ))
      }
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 0,
    marginTop: 20,
  },

  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 10,
  },
});
