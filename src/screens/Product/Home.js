import React , {useMemo, useState,useEffect} from "react";
import { ScrollView } from "react-native";
import StatusBar from "../../components/StatusBar";
import Search from "../../components/Search";
import NewProducts from "../../components/Home/Products1";
import colors from "../../styles/colors";
import {getIdUserApi} from "../../api/token";
import { API_URL } from "../../utils/constanst";


export default function Home() {





  return (
    <>
      <StatusBar backgroundColor={colors.bgDark} barStyle="light-content" />
      <Search />
      <ScrollView>
        
        <NewProducts />
      </ScrollView>
    </>
  );
}