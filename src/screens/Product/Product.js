import React, {useState,useEffect} from "react";
import { StyleSheet,Image,Text,View, ScrollView,Button } from "react-native";
import StatusBar from "../../components/StatusBar";
import Search from "../../components/Search";
import ScreenLoading from "../../components/ScreenLoading";
import { map } from "lodash";
import { API_URL_IMAGES } from "../../utils/constanst";
import { RadioButton } from 'react-native-paper';
import Buy from "../../components/Product/Buy";
import Quantity from "../../components/Product/Quantity";
import {getProductApi} from "../../api/product";
import {getProductExistenciaApi} from "../../api/product";
import {getTokenApi} from "../../api/token";
import {getCartId} from "../../api/product";
import jwtDecode from "jwt-decode";
import colors from "../../styles/colors";


export default function Product(props) {
const {route} = props;
const {params} = route;

const [product, setProduct] = useState(null);
const [productExistencia, setProductExistencia] = useState(null);
const [quantity, setQuantity] = useState(1);  
const [value, setValue] = React.useState('KG');
const [idUsuario, setIdUsuario] = useState('');
const [idCart, setIdCart] = useState(null);  


useEffect(() => {
  (async () => {
    const token = await getTokenApi();

    if (token) {

      setIdUsuario(
        jwtDecode(token).sub,
      );
    } else {
      setIdUsuario(null);
    }
  })();
}, []);


useEffect(() => {
    (async () =>{
        const response = await getProductApi(params.idProduct,params.idCentroVenta);
        setProduct(response); 

    })()
}, [params])

useEffect(() => {
  (async () =>{
      const response = await getProductExistenciaApi(params.idProduct
        );
        setProductExistencia(response); 
     

  })()
}, [params])

useEffect(() => {
  (async () =>{
      const response = await getCartId(idUsuario);
      setIdCart(response); 

  })()
}, [])

    return (
        <>
        <StatusBar  backgroundColor={colors.bgDark} barstyle="light-content"/>
        <Search/>
        {!product ? (
          <ScreenLoading text="Cargando producto" size="large"/>
        ) :(
            <ScrollView style={styles.container}>
                
                <Text style={styles.title}>{product.product.name}</Text>
                <Image
                style={styles.image}
                source={{
                  uri: `${API_URL_IMAGES}${params.imagen}`,
                }}
              />
              <Text style={styles.precio}>Precio:${params.precios}/kg</Text>
              
              <Quantity quantity= {quantity} setQuantity= {setQuantity}  /> 
              
              <RadioButton.Group onValueChange={value => setValue(value)} value={value} >
      <RadioButton.Item label="KG" value="KG" />
      <RadioButton.Item label="CAJA" value="Caja" />
    </RadioButton.Group> 
            
           <Buy product={product} quantity={quantity} productExistencia={productExistencia} value={value} idCart={idCart} idUsuario={idUsuario}/>
        </ScrollView>
        )}
        
        </>
    );
}


const styles = StyleSheet.create({
    container : {
        padding: 10,
        paddingBottom: 50,
    },
    title: {
    fontWeight: "bold",
    fontSize:20,
    marginBottom:20,
},
image: {
    width: 400,
    height: 400,
    resizeMode: "contain",
  },
  precio: {

    fontSize: 26,
    fontWeight: "bold",
    alignSelf: "center",
    color: "#000",
    marginTop: 20,
  },
  containerView:{
    padding:10,
    paddingBottom:200,
  },
  radioButton:{
    flexDirection: 'row',
    alignSelf:"baseline",
    paddingHorizontal:11,
    paddingVertical:7

  },

});