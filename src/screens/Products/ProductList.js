import React, { useState, useEffect } from "react";
import { Text, Modal, View, Button, ScrollView, StyleSheet, CheckBox, TextInput, Platform, TouchableOpacity, SafeAreaView, ActivityIndicator, FlatList } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Icon from '@expo/vector-icons/AntDesign';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useNavigation } from '@react-navigation/native';
import ScreenLoading from "../../components/ScreenLoading";
import { getProductApi } from "../../api/product";
import colors from "../../styles/colors";
import Route2 from "../../components/Products/Route2";
import jwtDecode from "jwt-decode";
import {getTokenApi} from "../../api/token";
import StatusBar from "../../components/StatusBar";
import Search from "../../components/Search";

export default function ProductList(props) {
    const { route } = props;
    const { params } = route;
    const [Indexs1, setIndexs1] = useState(null);
    const [idUser, setIdUser] = useState(undefined);
    useEffect(() => {
        (async () => {
          const token = await getTokenApi();
      
          if (token) {
      
            setIdUser({
              userid:jwtDecode(token).sub,
           
            });
          } else {
            setIdUser(null);
          }
          const response = await getProductApi(params.idproduct,jwtDecode(token).sub);
          setIndexs1(response);
              
        })();
      }, []);
     
 
    return (
        <>
              <StatusBar backgroundColor={colors.bgDark} barStyle="light-content" />
              <Search />
            {!Indexs1 ? (
                <ScreenLoading text="Cargando producto" size="large" />
            ) : (
              <ScrollView>
                <Route2 Indexs1={Indexs1} />
              </ScrollView>
            )}

        </>
    );
}


const styles = StyleSheet.create({
    container: {
        padding: 10,
        paddingBottom: 50,
    },
    title: {
        fontWeight: "bold",
        fontSize: 20,
        marginBottom: 20,
        color: "#fff"
    }

});