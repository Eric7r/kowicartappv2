import React from 'react';
import { Text, View, Image, TextInput } from 'react-native';
import Icon from '@expo/vector-icons/AntDesign';
import Icon2 from '@expo/vector-icons/MaterialIcons';

export default class Credit extends React.Component {

  render() {
    const { navigate } = this.props.navigation
    return (
      /*Div principal*/
      <View style={{ backgroundColor: "#fff", height: "100%" }}>

        {/*Titulo*/}
        <Text style={{
          fontSize: 20,
          fontFamily: "SemiBold",
          marginTop: 100,
          alignSelf: "center",
          color: "#808080"
        }}>Credito del usuario</Text>




        {/* Saldo maximo */}
        <View style={{
          flexDirection: "row",
          alignItems: "center",
          marginHorizontal: 55,
          marginTop: 15,
          paddingHorizontal: 10,
          paddingVertical: 2,
          width: "100%"

        }}>
          <Icon2 name="attach-money" color='#FF7709' size={24} style={{
            marginRight: 5,
            marginLeft: 0,
          }} />

          <Text style={{
            fontSize: 14,
            fontFamily: "SemiBold",
            alignSelf: "center",
            color: "#808080",
            marginRight: 10
          }}>Saldo maximo:</Text>

          <Text style={{
            fontSize: 14,
            fontFamily: "SemiBold",
            alignSelf: "center",
            color: "#000000"
          }}>0.00
            </Text>
        </View>


        {/* Saldo utilizado */}
        <View style={{
          flexDirection: "row",
          alignItems: "center",
          marginHorizontal: 55,
          marginTop: 15,
          paddingHorizontal: 10,
          paddingVertical: 2,
          width: "100%"

        }}>
          <Icon2 name="attach-money" color='#FF7709' size={24} style={{
            marginRight: 5,
            marginLeft: 0,
          }} />

          <Text style={{
            fontSize: 14,
            fontFamily: "SemiBold",
            alignSelf: "center",
            color: "#808080",
            marginRight: 10
          }}>Saldo utilizado:</Text>

          <Text style={{
            fontSize: 14,
            fontFamily: "SemiBold",
            alignSelf: "center",
            color: "#000000"
          }}>0.00
            </Text>
        </View>

        {/* Saldo disponible */}
        <View style={{
          flexDirection: "row",
          alignItems: "center",
          marginHorizontal: 55,
          marginTop: 15,
          paddingHorizontal: 10,
          paddingVertical: 2,
          width: "100%"

        }}>
          <Icon2 name="attach-money" color='#FF7709' size={24} style={{
            marginRight: 5,
            marginLeft: 0,
          }} />

          <Text style={{
            fontSize: 14,
            fontFamily: "SemiBold",
            alignSelf: "center",
            color: "#808080",
            marginRight: 10
          }}>Saldo disponible:</Text>

          <Text style={{
            fontSize: 14,
            fontFamily: "SemiBold",
            alignSelf: "center",
            color: "#000000"
          }}>0.00
            </Text>
        </View>




        {/* Politica 1 */}
        <Text onPress={() => navigate('Login')}
          style={{
            fontSize: 11,
            color: "#5C5C5C",
            fontFamily: "SemiBold",
            textAlign: "center",
            marginHorizontal: 55,
            marginTop: 30,
            opacity: 0.8,

            marginBottom: 0,

          }}>Alimentos Kowi 2021</Text>

        {/* Politica 2 */}
        <Text style={{
          fontSize: 9,
          color: "#0083CA",
          fontFamily: "SemiBold",
          textAlign: "center",
          marginHorizontal: 55,
          marginTop: 0,
          opacity: 0.8,
          marginBottom: 0,

        }}>Politica de privacidad</Text>

      </View>

    )
  }
}


