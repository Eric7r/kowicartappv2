import React from 'react'
import { StyleSheet, View } from "react-native";
import { TextInput, Button } from "react-native-paper";
import StatusBar from "../../components/StatusBar";
import colors from "../../styles/colors";
import { formStyle } from "../../styles";

export default function changeName() {
    return (
          <>
        <StatusBar backgroundColor={colors.bgDark} barStyle="light-content" />
        <View style={styles.container}>
          <TextInput
            label="Nombre"
            style={formStyle.input}
          
          />
         
          <Button
            mode="contained"
            style={formStyle.btnSucces}
         
          >
            Cambiar nombre y apellidos
          </Button>
        </View>
      </>
    )
}



var styles = StyleSheet.create({
    container: {
      padding: 20,
    },
  });