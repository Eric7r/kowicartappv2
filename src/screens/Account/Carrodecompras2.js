import React, { useState, useEffect } from "react";
import {
  Text,
  Modal,
  View,
  Button,
  ScrollView,
  StyleSheet,
  CheckBox,
  TextInput,
  Platform,
  TouchableOpacity,
  SafeAreaView,
  ActivityIndicator,
  FlatList,
} from "react-native";
import ScreenLoading from "../../components/ScreenLoading";
import Carrodecompras from "../../screens/Account/Carrodecompras";

export default function Carrodecompras2() {
  

  return (
    <>
    <ScrollView>     
        <Carrodecompras/>
    </ScrollView>     
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    paddingBottom: 50,
  },
  title: {
    fontWeight: "bold",
    fontSize: 20,
    marginBottom: 20,
    color: "#fff",
  },
});
